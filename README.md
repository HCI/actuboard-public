# ActuBoard v1.1

The ActuBoard Project is structured as followed:

1) **ActuBoard Firmware**
	C++ firmware for the ESP32 and the ActuBoard specific hardware (Controller + Hubs). Also provide the serial communication interface on hardware side, as well as the addressing of each Hub port through I2C.

2) **ActuBoard API**

	2.1) ActuBoard Interface
	Unity compatible C# library for serial communication with ActuBoard. Can be imported easily as DLL in own projects.
		
	2.2) ActuBoard Console
	Debugging tool that relies on the ActuBoard Interface. Good to test commands without having a project ready. Also, includes a terminal-like output which makes Putty or similar not necessary.
	
3) **Schematics**
Contains schematics, renderings, list of commands and everything necessary to rebuild ActuBoard.