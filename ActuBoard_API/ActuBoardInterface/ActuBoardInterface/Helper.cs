﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActuBoardAPI
{
    class Helper
    {

        public static byte[] IntToHexByte(int i)
        {
            byte[] intBytes = BitConverter.GetBytes(i);
            if (BitConverter.IsLittleEndian)
                Array.Reverse(intBytes);
            return intBytes;
        }

        public static String IntToHex(int i)
        {
            return i.ToString("X2");
        }


        public static String IntArrayToNumHexString(int[] cids, bool leadingCnt)
        {
            String channels = "";
            if (cids.Length > 0)
            {
                if(leadingCnt)
                    channels += Helper.IntToHex(cids.Length);
                foreach (int c in cids)
                    channels += Helper.IntToHex(c);
            }
            return channels;
        }

        public static String IntArrayToNumHexString(int[] cids)
        {
            return IntArrayToNumHexString(cids, true);
        }

    }
}
