﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActuBoardAPI
{
    public interface IActuBoard
    {

        int Connect();
        int Disconnect();
        bool IsConnected();
        int SetComPort(int comport);

        int SetChannel(int cid, int pwm);
        int SetChannel(string cid, string pwm);
        int GetChannel(int cid);

        /// <summary> 
        /// Set multiple channels at once to a given pwm value.
        /// </summary> 
        /// <param name="cid"></param> 
        int SetChannels(int[] cid, int pwm);
        int SetChannels(int[] cid, int[] pwm);
        int SetBlinkingInterval(int interval);
        int SetBlinkingCycle(int cycle);
        int EnableBlinking(int cid);
        int DisableBlinking(int cid);
        int EnableBlinking(int[] cid);
        int DisableBlinking(int[] cid);
        void EnableOutput(bool enable);
        int ListDevices();
        void ToggleDebug();
        void SendCustomCommand(string cmd);
    }

}
