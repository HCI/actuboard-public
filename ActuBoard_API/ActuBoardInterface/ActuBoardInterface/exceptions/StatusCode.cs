﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActuBoardAPI
{
    public enum StatusCode
    {
        NoError,
        HardwareCommandError,
        ChannelInvalid,
        PwmValueInvalid,
        ActuBoardNotConnected,
        ActuBoardConnected,
        ActuBoardIsConnecting,
        ComPortInvalid
    }
}
