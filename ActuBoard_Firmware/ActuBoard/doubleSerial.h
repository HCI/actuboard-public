// safety againts double-include
#ifndef doubleSerial_h
#define doubleSerial_h
#include <Arduino.h>
#include <HardwareSerial.h>
#include "BluetoothSerial.h" 

#if !defined(CONFIG_BT_ENABLED) || !defined(CONFIG_BLUEDROID_ENABLED)
#error Bluetooth is not enabled! Please run `make menuconfig` to and enable it
#endif

// Stub extension for now
class doubleSerial {
    public:
        doubleSerial();
        doubleSerial(const char *BT_Name);
		void selectSerial(boolean useBluetooth);
		boolean isBT(void);
		
		boolean readSerial(uint8_t *data);
		void writeSerial(const char c);
		void writeSerial(const char *c);
		boolean hasClient(void);
    private:
    	boolean useBT;
    	const char *BTName = NULL;  
    	BluetoothSerial SerialBT;
    	
};
extern HardwareSerial Serial;

#endif
// *********** END OF CODE **********
