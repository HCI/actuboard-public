//Source: https://github.com/rambo/pca9635


// safety againts double-include
#ifndef pca9635_h
#define pca9635_h
#include <Arduino.h> 
#include <Wire.h>

#define NUM_CHANNELS		16
#define MODE_REGISTERS		4

// Stub extension for now
class pca9635 {
    public:
        pca9635(); // We need this so we can set default address to the all-call one for the PCA9635 instance
		
        void begin(byte dev_addr, boolean wire_begin);
        void begin(byte dev_addr, boolean wire_begin, boolean init); // Variant to allow skipiing init (needed by the RGB board)
        boolean set_led_mode(byte ledno, byte mode);
        boolean set_led_mode(byte mode); // Variant to set all leds to same mode
        boolean set_all_led_modes(uint8_t cnt, uint8_t leds[], uint8_t modes[]);
        boolean set_led_pwm(byte ledno, byte cycle);
        boolean set_all_led_pwm(uint8_t start_led, uint8_t cnt, uint8_t *cycles);
        boolean set_group_dimming(uint8_t cycle); //190.7Hz (5.24ms) signal, duty = cycle * 2 * 256 * 40ns
        boolean set_group_blinking(uint8_t freq); //freq * 41.6 ms
        boolean set_driver_mode(byte mode);
        boolean set_invert_output(byte mode);
       	boolean set_output_not_active_state(byte mode);
        boolean set_group_control(byte mode);
        boolean set_sleep(byte sleep);
        boolean enable_subaddr(byte addr);
        boolean isAvailable();
        uint8_t getPWMValue(uint8_t channel);
        
        boolean reset(); // NOTE: This resets all PCA9635 devices on the bus
        
    private:
    	uint8_t device_address;
    	uint8_t autoincrement_bits;
    	boolean available;
    	uint8_t pwm_values[NUM_CHANNELS];
    	uint8_t led_mode_regs[MODE_REGISTERS];
    	
    	boolean readSingleReg(uint8_t reg, uint8_t *data);
    	boolean writeSingleReg(uint8_t reg, uint8_t data);
    	boolean writeManyReg(uint8_t reg, uint8_t cnt, uint8_t *data);
    	void exchangeValueReg(uint8_t *reg, uint8_t mask, uint8_t value);
    	boolean readModifyWriteReg(uint8_t reg, uint8_t mask, uint8_t value);
    	boolean reset(uint8_t addr);
    	
};

extern pca9635 PCA9635;


#endif
// *********** END OF CODE **********
