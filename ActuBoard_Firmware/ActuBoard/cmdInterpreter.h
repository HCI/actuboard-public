// safety againts double-include
#ifndef cmdInterpreter_h
#define cmdInterpreter_h
#include <Arduino.h>
#include "doubleSerial.h"
#include "pca9635.h"


#define CMD_BUFFER_LENGTH	256 
#define CMD_CR				'\r'

typedef enum {
	RETURN_ERROR	= 7,
	RETURN_CR		= CMD_CR,
} return_code_t;

// Stub extension for now
class cmdInterpreter {
    public:
        cmdInterpreter();

		boolean addToBuffer(uint8_t d);
		return_code_t interprete(pca9635 pwm_driver[], uint8_t cnt, uint8_t oe_pin);
		void prepareForNextCommand();
    private:
    	uint8_t cmdBuffer[CMD_BUFFER_LENGTH];
    	uint8_t *cmdBuffer_ptr = cmdBuffer;
    	uint8_t debug = false;

		typedef enum {
			MODE_SINGLE_VALUE = 1,
			MODE_MULTIPLE_VALUE = 2,
		} interpreteMode_t;

    	return_code_t readParameters(uint8_t *cmd_buf_pntr, uint8_t cmd_len, uint8_t parameters[], uint8_t num_parameters);
    	return_code_t readParametersWithLength(uint8_t *cmd_buf_pntr, uint8_t cmd_len, uint8_t parameters[], uint8_t *num_parameters, cmdInterpreter::interpreteMode_t m);
    	
};

extern doubleSerial doubleS;

void PrintHex8(char *dstptr, uint8_t *srcdata, uint8_t length);
void PrintHex8(char *dstptr, boolean *srcdata, uint8_t length);
uint8_t ascii2byte(const uint8_t *val);

#endif
// *********** END OF CODE **********
